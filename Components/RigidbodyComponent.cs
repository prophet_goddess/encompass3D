﻿using BulletSharp;
using Encompass;

namespace Encompass3D.Components
{
    public struct RigidbodyComponent : IComponent
    {
        public RigidBody Rigidbody { get; }
        public CollisionGroups CollisionGroups { get; }
        public CollisionGroups CollisionMask { get; }

        public RigidbodyComponent(RigidBody rigidbody, CollisionGroups collisionGroups, CollisionGroups collisionMask)
        {
            Rigidbody = rigidbody;
            CollisionGroups = collisionGroups;
            CollisionMask = collisionMask;
        }
    }
}
