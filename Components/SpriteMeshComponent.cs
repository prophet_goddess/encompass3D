using Encompass;
using Kav;

namespace Encompass3D.Components
{
    public struct SpriteMeshComponent : IComponent
    {
        public SpriteMesh SpriteMesh { get; }
        public SpriteBillboardConstraint BillboardConstraint { get; }

        public SpriteMeshComponent(
            SpriteMesh spriteMesh,
            SpriteBillboardConstraint billboardConstraint
        ) {
            SpriteMesh = spriteMesh;
            BillboardConstraint = billboardConstraint;
        }
    }
}
