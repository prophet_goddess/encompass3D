using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Components
{
    public struct AmbientLightComponent : IComponent
    {
        public Color Color { get; }

        public AmbientLightComponent(Color color)
        {
            Color = color;
        }
    }
}