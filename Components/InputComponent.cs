﻿using Encompass;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Encompass3D.Components
{
    public struct Input
    {
        public Keys[] keys;
        public Buttons[] buttons;
        public Input(Keys[] keys, Buttons[] buttons)
        {
            this.keys = keys;
            this.buttons = buttons;
        }
    }

    // FIXME: everything about this is evil, strings are bad,
    public struct InputComponent : IComponent
    {
        public Dictionary<string, Input> inputs;
        public string leftMouseButton;
        public string rightMouseButton;
        public string middleMouseButton;
    }
}
