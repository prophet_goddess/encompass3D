using Encompass;

namespace Encompass3D.Components
{
    public struct PointLightComponent : IComponent
    {
        public Kav.PointLight PointLight { get; }

        public PointLightComponent(Kav.PointLight pointLight)
        {
            PointLight = pointLight;
        }
    }
}
