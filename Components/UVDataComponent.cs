using Encompass;
using Kav;

namespace Encompass3D.Components
{
    public struct UVDataComponent : IComponent
    {
        public UVData UVData { get; }

        public UVDataComponent(UVData uvData)
        {
            UVData = uvData;
        }
    }
}
