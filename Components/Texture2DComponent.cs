using Encompass;
using Microsoft.Xna.Framework.Graphics;

namespace Encompass3D.Components
{
    public struct Texture2DComponent : IComponent
    {
        public Texture2D Texture { get; }

        public Texture2DComponent(Texture2D texture)
        {
            Texture = texture;
        }
    }
}
