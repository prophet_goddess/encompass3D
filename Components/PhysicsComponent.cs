﻿using BulletSharp;
using Encompass;
using System.Collections.Generic;

namespace Encompass3D.Components
{
    public struct PhysicsComponent : IComponent
    {
        public DynamicsWorld World;
        public Dictionary<CollisionObject, Entity> CollisionObjects;

        public PhysicsComponent(DynamicsWorld world)
        {
            World = world;
            CollisionObjects = new Dictionary<CollisionObject, Entity>();
        }
    }
}
