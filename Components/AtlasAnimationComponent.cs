using System;
using Encompass;
using Kav;

namespace Encompass3D.Components
{
    public struct AtlasAnimationComponent : IComponent
    {
        public AtlasAnimation AtlasAnimation { get; }
        public int CurrentFrame { get; }
        public int PreviousFrame { get; }
        public bool Loop { get; }

        public double ElapsedTime { get; }

        public double LoopTime
        {
            get { return TimeOf(FrameCount); }
        }

        public UVData CurrentUVData { get { return AtlasAnimation.Frames[CurrentFrame]; } }

        public int FrameCount { get { return AtlasAnimation.Frames.Length; } }

        public AtlasAnimationComponent(AtlasAnimation atlasAnimation, bool loop)
        {
            AtlasAnimation = atlasAnimation;
            ElapsedTime = 0;
            PreviousFrame = -1;
            CurrentFrame = 0;
            Loop = loop;
        }

        public AtlasAnimationComponent(AtlasAnimationComponent animationComponent, double dt)
        {
            AtlasAnimation = animationComponent.AtlasAnimation;
            ElapsedTime = animationComponent.ElapsedTime + dt;
            PreviousFrame = animationComponent.CurrentFrame;
            Loop = animationComponent.Loop;
            CurrentFrame = FrameFromTime(ElapsedTime, AtlasAnimation.Framerate, AtlasAnimation.Frames.Length, animationComponent.Loop);
        }

        public double TimeOf(int frame)
        {
            return 1.0 / AtlasAnimation.Framerate * frame;
        }

        public bool AtEnd()
        {
            return CurrentFrame == FrameCount - 1;
        }

        private static int FrameFromTime(double time, int framerate, int frameCount, bool loop)
        {
            return loop ? ((int)(Math.Floor(time * framerate) % frameCount)) : Math.Min((int)Math.Floor(time * framerate), frameCount - 1);
        }
    }
}
