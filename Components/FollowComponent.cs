﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Components
{
    public struct FollowComponent : IComponent
    {
        public Entity Following { get; }
        public Vector3 Offset { get; }
        public Quaternion Orientation { get; }

        public FollowComponent(Entity folowing, Vector3 offset, Quaternion orientation)
        {
            Following = folowing;
            Offset = offset;
            Orientation = orientation;
        }
    }
}
