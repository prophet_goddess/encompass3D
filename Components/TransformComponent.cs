﻿using Microsoft.Xna.Framework;
using Encompass;

namespace Encompass3D.Components
{
    public struct TransformComponent : IComponent, System.IEquatable<TransformComponent>
    {
        public static TransformComponent Identity = new TransformComponent(Vector3.Zero, Quaternion.Identity, Vector3.One);

        public Matrix TransformMatrix { get; }
        public Vector3 Position { get; }
        public Quaternion Orientation { get; }
        public Vector3 Scale { get; }

        public Vector3 Forward { get { return TransformMatrix.Forward; } }
        public Vector3 Right { get { return TransformMatrix.Right; } }
        public Vector3 Up { get { return TransformMatrix.Up; } }

        public TransformComponent(Vector3 position, Quaternion orientation, Vector3 scale)
        {
            Position = position;
            Orientation = orientation;
            Scale = scale;
            TransformMatrix = CreateTransformMatrix(Position, Orientation, Scale);
        }

        public TransformComponent(Vector3 position, Quaternion orientation)
        {
            Position = position;
            Orientation = orientation;
            Scale = Vector3.One;
            TransformMatrix = CreateTransformMatrix(Position, Orientation, Scale);
        }

        public TransformComponent(Vector3 position)
        {
            Position = position;
            Orientation = Quaternion.Identity;
            Scale = Vector3.One;
            TransformMatrix = CreateTransformMatrix(Position, Orientation, Scale);
        }

        public TransformComponent WithPosition(Vector3 position)
        {
            return Translate(position - Position);
        }

        public TransformComponent Translate(Vector3 translation)
        {
            return new TransformComponent(Position + translation, Orientation, Scale);
        }

        public TransformComponent TranslateLocal(Vector3 translation)
        {
            return Translate(Vector3.Transform(translation, Orientation));
        }

        public TransformComponent RotateLocal(Quaternion other)
        {
            return new TransformComponent(Position, Orientation * other, Scale);
        }

        public TransformComponent RotateLocal(float yaw, float pitch, float roll)
        {
            return RotateLocal(Quaternion.CreateFromYawPitchRoll(
                yaw,
                pitch,
                roll
            ));
        }

        public TransformComponent RotateLocal(Vector3 eulerAngles)
        {
            return RotateLocal(Quaternion.CreateFromYawPitchRoll(
                eulerAngles.X,
                eulerAngles.Y,
                eulerAngles.Z
            ));
        }

        public TransformComponent Compose(TransformComponent other)
        {
            return new TransformComponent(Position + other.Position, Orientation * other.Orientation, Scale * other.Scale);
        }

        private static Matrix CreateTransformMatrix(Vector3 position, Quaternion orientation, Vector3 scale)
        {
            return Matrix.CreateScale(scale) *
                   Matrix.CreateFromQuaternion(orientation) *
                   Matrix.CreateTranslation(position);
        }

        public override bool Equals(object other)
        {
            if (other is TransformComponent otherTransform)
            {
                return Equals(otherTransform);
            }

            return false;
        }

        public bool Equals(TransformComponent other)
        {
            return TransformMatrix == other.TransformMatrix;
        }

        public override int GetHashCode()
        {
            return TransformMatrix.GetHashCode();
        }

        public static bool operator ==(TransformComponent a, TransformComponent b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(TransformComponent a, TransformComponent b)
        {
            return !(a == b);
        }
    }
}
