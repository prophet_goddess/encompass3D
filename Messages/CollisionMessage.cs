﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct CollisionMessage : IMessage
    {
        public Entity Hitter { get; }
        public Entity Hit { get; }
        public Vector3 HitLocation { get; }
        public Vector3 HitNormal { get; }

        public CollisionMessage(Entity hitter, Entity hit, Vector3 hitLocation, Vector3 hitNormal)
        {
            Hitter = hitter;
            Hit = hit;
            HitLocation = hitLocation;
            HitNormal = hitNormal;
        }
    }
}
