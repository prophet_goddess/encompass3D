﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct RaycastMessage : IMessage
    {
        public Entity Hitter { get; }
        public Vector3 From { get; }
        public Vector3 To { get;  }
        public CollisionGroups CollisionGroups { get; }
        public CollisionGroups CollisionMask { get; }

        public RaycastMessage(
            Entity hitter,
            Vector3 from,
            Vector3 to,
            CollisionGroups collisionGroups = CollisionGroups.All,
            CollisionGroups collisionMask = CollisionGroups.All
        ) {
            Hitter = hitter;
            From = from;
            To = to;
            CollisionGroups = collisionGroups;
            CollisionMask = collisionMask;
        }
    }
}
