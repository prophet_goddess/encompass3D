﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetFollowMessage : IMessage
    {
        public Entity Following { get; }
        public Entity Follower { get; }
        public Vector3 Offset { get; }
        public Quaternion Orientation { get; }

        public SetFollowMessage(Entity following, Entity follower, Vector3 offset, Quaternion orientation)
        {
            Offset = offset;
            Following = following;
            Follower = follower;
            Orientation = orientation;
        }
    }
}
