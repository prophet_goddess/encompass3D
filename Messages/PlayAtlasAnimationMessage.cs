using Encompass;
using Kav;

namespace Encompass3D.Messages
{
    public struct PlayAtlasAnimationMessage : IMessage
    {
        public Entity Entity { get; }
        public AtlasAnimation Animation { get; }
        public bool Loop { get; }

        public PlayAtlasAnimationMessage(Entity entity, AtlasAnimation animation, bool loop)
        {
            Entity = entity;
            Animation = animation;
            Loop = loop;
        }
    }
}
