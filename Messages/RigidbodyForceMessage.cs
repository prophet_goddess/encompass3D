﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct RigidbodyForceMessage : IMessage
    {
        public Entity Entity { get; }
        public Vector3 Force { get; }

        public RigidbodyForceMessage(Entity entity, Vector3 force)
        {
            Entity = entity;
            Force = force;
        }
    }
}
