﻿using BulletSharp;
using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct InitializePhysicsMessage : IMessage
    {
        public DynamicsWorld world;

    }
}
