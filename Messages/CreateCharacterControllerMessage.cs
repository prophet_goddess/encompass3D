using Encompass;

namespace Encompass3D.Messages
{
    public struct CreateCharacterControllerMessage : IMessage
    {
        public Entity entity;
        public float lookSensitivity;
        public float speed;
        public float jumpForce;
        public int maxJumps;
    }
}
