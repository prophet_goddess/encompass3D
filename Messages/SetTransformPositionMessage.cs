using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetTransformPositionMessage : IMessage, IHasEntity
    {
        public Entity Entity { get; }
        public Vector3 Position { get; }

        public SetTransformPositionMessage(Entity entity, Vector3 position)
        {
            Entity = entity;
            Position = position;
        }
    }
}
