using Encompass;

namespace Encompass3D.Messages
{
    public struct DestroyStaticObjectMessage : IMessage
    {
        public Entity Entity { get; }

        public DestroyStaticObjectMessage(Entity entity)
        {
            Entity = entity;
        }
    }
}
