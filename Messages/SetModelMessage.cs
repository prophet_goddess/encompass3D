﻿using Encompass;
using Kav;

namespace Encompass3D.Messages
{
    public struct SetModelMessage : IMessage
    {
        public Entity entity;
        public Model model;
    }
}
