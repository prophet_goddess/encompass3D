using Encompass;
using BulletSharp;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct CreateStaticObjectMessage : IMessage
    {
        public Entity Entity { get; }
        public CollisionShape Shape { get; }
        public Vector3 Position { get; }
        public CollisionGroups FilterGroups { get; }
        public CollisionGroups CollisionMask { get; }

        public CreateStaticObjectMessage(
            Entity entity,
            CollisionShape shape,
            Vector3 position,
            CollisionGroups filterGroups = CollisionGroups.All,
            CollisionGroups collisionMask = CollisionGroups.All
        ) {
            Entity = entity;
            Shape = shape;
            Position = position;
            FilterGroups = filterGroups;
            CollisionMask = collisionMask;
        }
    }
}
