﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct RotateTransformMessage : IMessage
    {
        public Entity entity;
        public Quaternion delta;
    }
}
