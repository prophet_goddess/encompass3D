using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetTransformOrientationMessage : IMessage, IHasEntity
    {
        public Entity Entity { get; }
        public Quaternion Orientation { get; }

        public SetTransformOrientationMessage(Entity entity, Quaternion orientation)
        {
            Entity = entity;
            Orientation = orientation;
        }
    }
}
