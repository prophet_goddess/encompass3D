using Encompass;

namespace Encompass3D.Messages
{
    public struct CharacterMoveSpeedMessage : IMessage
    {
        public float Speed { get; }

        public CharacterMoveSpeedMessage(float speed)
        {
            Speed = speed;
        }
    }
}
