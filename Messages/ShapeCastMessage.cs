using BulletSharp;
using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Components
{
    public struct ShapeCastMessage : IMessage
    {
        public Entity Hitter { get; }
        public CollisionShape Shape { get; }
        public Vector3 Position { get; }
        public CollisionGroups CollisionGroups { get; }
        public CollisionGroups CollisionMask { get; }

        public ShapeCastMessage(
            Entity hitter,
            CollisionShape shape,
            Vector3 position,
            CollisionGroups collisionGroups = CollisionGroups.All,
            CollisionGroups collisionMask = CollisionGroups.All
        ) {
            Hitter = hitter;
            Shape = shape;
            Position = position;
            CollisionGroups = collisionGroups;
            CollisionMask = collisionMask;
        }
    }
}
