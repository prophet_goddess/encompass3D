using Encompass;
using Kav;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetPointLightMessage : IMessage
    {
        public Entity Entity { get; }
        public Vector3 Position { get; }
        public Color Color { get; }
        public float Radius { get; }

        public SetPointLightMessage(Entity entity, Vector3 position, Color color, float radius)
        {
            Entity = entity;
            Position = position;
            Color = color;
            Radius = radius;
        }
    }
}
