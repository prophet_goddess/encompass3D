﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct RaycastAllMessage : IMessage
    {
        public Vector3 From { get; }
        public Vector3 To { get; }

        public RaycastAllMessage(Vector3 from, Vector3 to)
        {
            From = from;
            To = to;
        }
    }
}
