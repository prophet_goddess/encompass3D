﻿using BulletSharp;
using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SweepTestMessage : IMessage
    {
        Matrix From { get; }
        Matrix To { get; }

        CollisionShape collisionShape;
    }
}
