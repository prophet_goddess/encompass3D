﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Encompass3D.Utility;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Encompass3D.Messages
{
    public struct FollowMessage : IHasEntity, IMessage
    {
        public Entity Entity { get; }

        public Entity Follower { get; }

        public FollowMessage(Entity entity, Entity follower)
        {
            Entity = entity;
            Follower = follower;
        }

    }
}
