﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct RigidbodyTorqueMessage : IMessage
    {
        public Entity entity;
        public Vector3 vector;
    }
}
