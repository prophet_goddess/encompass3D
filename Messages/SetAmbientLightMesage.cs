using Encompass;
using Kav;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetAmbientLightMessage : IMessage
    {
        public Entity Entity { get; }
        public Color Color { get; }

        public SetAmbientLightMessage(Entity entity, Color color)
        {
            Entity = entity;
            Color = color;
        }
    }
}
