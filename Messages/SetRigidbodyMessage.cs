﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetRigidbodyMessage : IMessage
    {
        public Entity Entity { get; }
        public Vector3 Position { get; }

        public SetRigidbodyMessage(Entity entity, Vector3 position)
        {
            Entity = entity;
            Position = position;
        }
    }
}
