﻿using Encompass; 

namespace Encompass3D.Messages
{
    public struct DestroyRigidbodyMessage : IMessage, IHasEntity
    {
        public Entity Entity { get; }

        public DestroyRigidbodyMessage(Entity entity)
        {
            Entity = entity;
        }
    }
}
