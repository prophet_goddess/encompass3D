﻿using Encompass;
using BulletSharp;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct CreateRigidbodyMessage : IMessage
    {
        public Entity Entity { get; }
        public RigidBodyConstructionInfo ConstructionInfo { get; }
        public Vector3 Position { get; }
        public Quaternion Orientation { get; }
        public Vector3 LinearFactor { get; }
        public Vector3 AngularFactor { get; }
        public Vector3 InitialImpulse { get; }
        public CollisionGroups CollisionGroups { get; }
        public CollisionGroups CollisionMask { get; }

        public CreateRigidbodyMessage(
            Entity entity,
            RigidBodyConstructionInfo constructionInfo,
            Vector3 position,
            Quaternion orientation,
            Vector3 linearFactor,
            Vector3 angularFactor,
            CollisionGroups filterGroups = CollisionGroups.All,
            CollisionGroups collisionMask = CollisionGroups.All
        ) {
            Entity = entity;
            ConstructionInfo = constructionInfo;
            Position = position;
            Orientation = orientation;
            LinearFactor = linearFactor;
            AngularFactor = angularFactor;
            InitialImpulse = Vector3.Zero;
            CollisionGroups = filterGroups;
            CollisionMask = collisionMask;
        }

        public CreateRigidbodyMessage(
            Entity entity,
            RigidBodyConstructionInfo constructionInfo,
            Vector3 position,
            Quaternion orientation,
            Vector3 linearFactor,
            Vector3 angularFactor,
            Vector3 initialImpulse,
            CollisionGroups filterGroups = CollisionGroups.All,
            CollisionGroups collisionMask = CollisionGroups.All
        ) {
            Entity = entity;
            ConstructionInfo = constructionInfo;
            Position = position;
            Orientation = orientation;
            LinearFactor = linearFactor;
            AngularFactor = angularFactor;
            InitialImpulse = initialImpulse;
            CollisionGroups = filterGroups;
            CollisionMask = collisionMask;
        }
    }
}
