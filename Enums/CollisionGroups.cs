using System;

namespace Encompass3D
{
    [Flags]
    public enum CollisionGroups
    {
        All = -1,
        None = 0,
        Terrain = 1,
        Player = 2,
        Enemies = 4,
        Debris = 8,
        Items = 16,
        Projectiles = 32
    }
}
