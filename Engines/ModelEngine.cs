﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;

namespace Encompass3D.Engines
{
    [Reads(typeof(ModelComponent))]
    [Writes(typeof(ModelComponent))]
    [Receives(typeof(SetModelMessage))]
    public class ModelEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (var setModel in ReadMessages<SetModelMessage>())
            {
                ModelComponent model;
                model.model = setModel.model;

                SetComponent(setModel.entity, model);
            }
        }
    }
}
