﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Encompass3D.Utility;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Encompass3D.Engines
{
    [QueryWith(typeof(TransformComponent))]
    [QueryWithout(typeof(StaticComponent))]
    [Reads(
        typeof(TransformComponent),
        typeof(RigidbodyComponent),
        typeof(FollowComponent)
    )]
    [Receives(
        typeof(SetTransformPositionMessage),
        typeof(SetTransformOrientationMessage),
        typeof(FollowMessage)
    )]
    [Writes(typeof(TransformComponent))]
    public class TransformEngine : Engine
    {
        private Dictionary<Entity, TransformComponent> transforms = new Dictionary<Entity, TransformComponent>();

        public override void Update(double dt)
        {
            transforms.Clear();

            foreach (var entity in TrackedEntities)
            {
                var transformComponent = GetComponent<TransformComponent>(entity);
                var position = transformComponent.Position;
                var orientation = transformComponent.Orientation;

                foreach (var setTransformPositionMessage in ReadMessagesWithEntity<SetTransformPositionMessage>(entity))
                {
                    position = setTransformPositionMessage.Position;
                }

                foreach (var SetTransformOrientationMessage in ReadMessagesWithEntity<SetTransformOrientationMessage>(entity))
                {
                    orientation = SetTransformOrientationMessage.Orientation;
                }

                transforms[entity] = new TransformComponent(position, orientation);
            }

            foreach (var followedEntity in TrackedEntities)
            {
                foreach (var followMessage in ReadMessagesWithEntity<FollowMessage>(followedEntity))
                {
                    var followedTransform = transforms.ContainsKey(followedEntity) ? transforms[followedEntity] : GetComponent<TransformComponent>(followedEntity);
                    var followComponent = GetComponent<FollowComponent>(followMessage.Follower);
                    transforms[followMessage.Follower] = new TransformComponent(
                        followedTransform.Position + followComponent.Offset,
                        followComponent.Orientation
                    );
                }
            }

            foreach (var kv in transforms)
            {
                SetComponent(kv.Key, kv.Value);
            }
        }
    }
}
