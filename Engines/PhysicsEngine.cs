﻿using Encompass;
using Encompass3D.Messages;
using Encompass3D.Components;
using Encompass3D.Utility;
using BulletSharp;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Encompass3D.Engines
{

    [Reads(typeof(RigidbodyComponent), typeof(TransformComponent), typeof(PhysicsComponent), typeof(StaticObjectComponent))]
    [Writes(typeof(RigidbodyComponent),typeof(StaticObjectComponent))]
    [Receives(typeof(CreateRigidbodyMessage), typeof(RigidbodyForceMessage),
                typeof(RigidbodyTorqueMessage), typeof(SetRigidbodyMessage),
                typeof(RigidbodyImpulseMessage), typeof(ClearForcesMessage),
                typeof(CreateStaticObjectMessage))]
    [Sends(typeof(SetTransformPositionMessage))]
    public class PhysicsEngine : Engine
    {
        private Dictionary<Entity, RigidbodyComponent> rigidbodies;

        public override void Update(double dt)
        {
            var physics = ReadComponent<PhysicsComponent>();

            foreach (var createRigidbody in ReadMessages<CreateRigidbodyMessage>())
            {
                var matrix = Matrix.CreateFromQuaternion(createRigidbody.Orientation) *
                            Matrix.CreateTranslation(createRigidbody.Position.X, createRigidbody.Position.Y, createRigidbody.Position.Z);

                createRigidbody.ConstructionInfo.MotionState = new DefaultMotionState(matrix.ToBulletMatrix());
                createRigidbody.ConstructionInfo.AngularSleepingThreshold = 0f;
                createRigidbody.ConstructionInfo.LinearSleepingThreshold = 0f;
                
                var rigidbody = new RigidBody(createRigidbody.ConstructionInfo);

                rigidbody.LinearFactor = createRigidbody.LinearFactor.ToBulletVector();
                rigidbody.AngularFactor = createRigidbody.AngularFactor.ToBulletVector();

                physics.World.AddRigidBody(rigidbody, (int)createRigidbody.CollisionGroups, (int)createRigidbody.CollisionMask);
                physics.CollisionObjects.Add(rigidbody, createRigidbody.Entity);

                rigidbody.ApplyCentralImpulse(createRigidbody.InitialImpulse.ToBulletVector());

                SetComponent(createRigidbody.Entity, new RigidbodyComponent(
                    rigidbody,
                    createRigidbody.CollisionGroups,
                    createRigidbody.CollisionMask
                ));

            }

            foreach (var createStatic in ReadMessages<CreateStaticObjectMessage>())
            {
                var staticObject = new CollisionObject
                {
                    CollisionShape = createStatic.Shape,
                    WorldTransform = BulletSharp.Math.Matrix.Translation(createStatic.Position.ToBulletVector()),
                    CollisionFlags = CollisionFlags.StaticObject,
                    ActivationState = ActivationState.IslandSleeping,
                };

                physics.World.AddCollisionObject(
                    staticObject,
                    (int)createStatic.FilterGroups,
                    (int)createStatic.CollisionMask
                );

                physics.CollisionObjects.Add(staticObject, createStatic.Entity);

                StaticObjectComponent staticObjectComponent;
                staticObjectComponent.collisionObject = staticObject;
                staticObjectComponent.inWorld = true;
                SetComponent(createStatic.Entity, staticObjectComponent);
            }

            if (rigidbodies == null)
            {
                rigidbodies = new Dictionary<Entity, RigidbodyComponent>();
            }
            else
            {
                rigidbodies.Clear();
            }

            var rigidbodyEntities = ReadEntities<RigidbodyComponent>();
            foreach (var entity in rigidbodyEntities)
            {
                var rigidbody = GetComponent<RigidbodyComponent>(entity);

                rigidbodies.Add(entity, rigidbody);
            }

            foreach (var clearForces in ReadMessages<ClearForcesMessage>())
            {
                rigidbodies[clearForces.entity].Rigidbody.ClearForces();
            }

            foreach (var setRigidbody in ReadMessages<SetRigidbodyMessage>())
            {
                var rigidbody = rigidbodies[setRigidbody.Entity];

                var matrix = 
                    Matrix.CreateTranslation(
                        setRigidbody.Position.X, 
                        setRigidbody.Position.Y, 
                        setRigidbody.Position.Z
                    );

                rigidbody.Rigidbody.WorldTransform = matrix.ToBulletMatrix();
                rigidbody.Rigidbody.MotionState.WorldTransform = matrix.ToBulletMatrix();
                rigidbody.Rigidbody.ClearForces();
            }

            foreach (var applyForce in ReadMessages<RigidbodyForceMessage>())
            {
                var rigidbody = rigidbodies[applyForce.Entity];
                rigidbody.Rigidbody.ApplyCentralForce(applyForce.Force.ToBulletVector());

            }

            foreach (var impulse in ReadMessages<RigidbodyImpulseMessage>())
            {
                var rigidbody = rigidbodies[impulse.entity];
                rigidbody.Rigidbody.ApplyCentralImpulse(impulse.vector.ToBulletVector());
            }

            physics.World.StepSimulation((float)dt);

            foreach (var kv in rigidbodies)
            {
                var entity = kv.Key;
                var rigidbody = kv.Value;

                SendMessage(new SetTransformPositionMessage(
                    entity,
                    kv.Value.Rigidbody.CenterOfMassPosition.ToXnaVector()
                ));
            }
        }
    }
}
