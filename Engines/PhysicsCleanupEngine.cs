﻿using Encompass;
using Encompass3D.Messages;
using Encompass3D.Components;

namespace Encompass3D.Engines
{
    [Reads(typeof(RigidbodyComponent), typeof(PhysicsComponent), typeof(StaticObjectComponent))]
    [Receives(typeof(DestroyRigidbodyMessage), typeof(DestroyStaticObjectMessage))]
    public class PhysicsCleanupEngine : Engine
    {
        public override void Update(double dt)
        {
            var physicsEntity = ReadEntity<PhysicsComponent>();
            var physics = GetComponent<PhysicsComponent>(physicsEntity);

            // FIXME: use ReadMessagesWithEntity
            foreach (var destroyRigidbody in ReadMessages<DestroyRigidbodyMessage>())
            {
                var rigidbody = GetComponent<RigidbodyComponent>(destroyRigidbody.Entity);
                physics.World.RemoveCollisionObject(rigidbody.Rigidbody);
                physics.CollisionObjects.Remove(rigidbody.Rigidbody);
                Destroy(destroyRigidbody.Entity);
            }

            foreach (var destroyStaticObject in ReadMessages<DestroyStaticObjectMessage>())
            {
                var staticObject = GetComponent<StaticObjectComponent>(destroyStaticObject.Entity);
                physics.World.RemoveCollisionObject(staticObject.collisionObject);
                physics.CollisionObjects.Remove(staticObject.collisionObject);
                Destroy(destroyStaticObject.Entity);
            }
        }
    }
}
