using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Microsoft.Xna.Framework.Graphics;

namespace Encompass3D.Engines
{
    [Reads(typeof(PointLightComponent), typeof(AmbientLightComponent), typeof(DirectionalLightComponent))]
    [Writes(typeof(PointLightComponent), typeof(AmbientLightComponent), typeof(DirectionalLightComponent))]
    [Receives(typeof(SetPointLightMessage), typeof(SetAmbientLightMessage), typeof(SetDirectionalLightMessage))]
    public class LightEngine : Engine
    {
        private GraphicsDevice GraphicsDevice { get; }
        private int BakedShadowMapSize { get; }

        public LightEngine(GraphicsDevice graphicsDevice, int bakedShadowMapSize)
        {
            GraphicsDevice = graphicsDevice;
            BakedShadowMapSize = bakedShadowMapSize;
        }

        public override void Update(double dt)
        {

            foreach (var setLight in ReadMessages<SetPointLightMessage>())
            {
                SetComponent(setLight.Entity, new PointLightComponent(
                    new Kav.PointLight(
                        GraphicsDevice,
                        setLight.Position,
                        setLight.Color,
                        setLight.Radius,
                        BakedShadowMapSize
                    )
                ));
            }

            foreach (var setLight in ReadMessages<SetAmbientLightMessage>())
            {
                SetComponent(setLight.Entity, new AmbientLightComponent(setLight.Color));

            }

            foreach (var setLight in ReadMessages<SetDirectionalLightMessage>())
            {
                DirectionalLightComponent directionalLight;
                directionalLight.color = setLight.color;
                directionalLight.intensity = setLight.intensity;
            }

        }
    }
}
