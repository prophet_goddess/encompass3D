﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Encompass3D.Utility;
using System.Collections.Generic;

namespace Encompass3D.Engines
{
    [Writes(typeof(FollowComponent))]
    [Reads(typeof(FollowComponent))]
    [Sends(typeof(FollowMessage))]
    [Receives(typeof(SetFollowMessage))]
    public class FollowEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (var follow in ReadEntities<FollowComponent>())
            {
                var followComponent = GetComponent<FollowComponent>(follow);
                SendMessage(new FollowMessage(followComponent.Following, follow));
            }

            foreach(var setFollow in ReadMessages<SetFollowMessage>())
            {
                SetComponent(setFollow.Follower, new FollowComponent(setFollow.Following, setFollow.Offset, setFollow.Orientation));
            }
        }
    }
}
