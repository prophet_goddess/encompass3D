using BulletSharp;
using Encompass;
using Encompass3D.Messages;
using Encompass3D.Utility;

namespace Encompass3D.Components
{
    [Reads(typeof(PhysicsComponent))]
    [Receives(typeof(RaycastMessage), typeof(RaycastAllMessage), typeof(ShapeCastMessage))]
    [Sends(typeof(CollisionMessage))]
    public class PhysicsCollisionEngine : Engine
    {
        private ContactResults contactResults = new ContactResults();

        public override void Update(double dt)
        {
            var physics = ReadComponent<PhysicsComponent>();

            foreach(var raycast in ReadMessages<RaycastMessage>())
            {
                var from = raycast.From.ToBulletVector();
                var to = raycast.To.ToBulletVector();

                ClosestRayResultCallback results = new ClosestRayResultCallback(ref from, ref to);
                results.CollisionFilterGroup = (int)raycast.CollisionGroups;
                results.CollisionFilterMask = (int)raycast.CollisionMask;

                physics.World.RayTest(raycast.From.ToBulletVector(), raycast.To.ToBulletVector(), results);

                if (results.HasHit)
                {
                    SendMessage(new CollisionMessage(
                        raycast.Hitter,
                        physics.CollisionObjects[results.CollisionObject],
                        results.HitPointWorld.ToXnaVector(),
                        results.HitNormalWorld.ToXnaVector()
                    ));
                }
            }

            foreach (var shapeCast in ReadMessages<ShapeCastMessage>())
            {
                var position = shapeCast.Position;

                var shape = shapeCast.Shape;
                var shapeObject = new CollisionObject
                {
                    CollisionShape = shape,
                    WorldTransform = BulletSharp.Math.Matrix.Translation(position.ToBulletVector()),
                    CollisionFlags = CollisionFlags.StaticObject,
                    ActivationState = ActivationState.ActiveTag
                };

                physics.World.AddCollisionObject(
                    shapeObject,
                    (int)shapeCast.CollisionGroups,
                    (int)shapeCast.CollisionMask
                );

                contactResults.Clear();
                contactResults.TestingObject = shapeObject;
                contactResults.CollisionFilterGroup = (int)shapeCast.CollisionGroups;
                contactResults.CollisionFilterMask = (int)shapeCast.CollisionMask;

                physics.World.ContactTest(shapeObject, contactResults);
                physics.World.RemoveCollisionObject(shapeObject);

                foreach (var (collisionObject, collisionPosition, collisionNormal) in contactResults.CollidedObjects())
                {
                    SendMessage(new CollisionMessage(
                        shapeCast.Hitter,
                        physics.CollisionObjects[collisionObject],
                        collisionPosition,
                        collisionNormal
                    ));
                }
            }
        }
    }
}
