using Encompass;
using Encompass3D.Messages;

namespace Encompass3D.Components
{
    [Receives(typeof(PlayAtlasAnimationMessage))]
    [Reads(typeof(AtlasAnimationComponent), typeof(UVDataComponent))]
    [Writes(typeof(AtlasAnimationComponent), typeof(UVDataComponent))]
    public class AtlasAnimationEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (ref readonly var entity in ReadEntities<AtlasAnimationComponent>())
            {
                var animationComponent = GetComponent<AtlasAnimationComponent>(entity);

                SetComponent(entity, new AtlasAnimationComponent(
                    animationComponent,
                    dt
                ));

                if (HasComponent<UVDataComponent>(entity))
                {
                    SetComponent(entity, new UVDataComponent(
                        animationComponent.CurrentUVData
                    ));
                }
            }

            foreach (ref readonly var message in ReadMessages<PlayAtlasAnimationMessage>())
            {
                var animationComponent = new AtlasAnimationComponent(
                    message.Animation,
                    message.Loop
                );

                SetComponent(message.Entity, animationComponent);
                SetComponent(message.Entity, new UVDataComponent(
                    animationComponent.CurrentUVData
                ));
            }
        }
    }
}
