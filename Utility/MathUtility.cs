﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encompass3D.Utility
{
    //conversion functions for passing data between bullet and fna
    public static class MathUtility
    {
        public static BulletSharp.Math.Quaternion ToBulletQuaternion(this Microsoft.Xna.Framework.Quaternion quaternion)
        {
            return new BulletSharp.Math.Quaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
        }

        public static Microsoft.Xna.Framework.Quaternion ToXnaQuaternion(this BulletSharp.Math.Quaternion quaternion)
        {
            return new Microsoft.Xna.Framework.Quaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
        }

        public static BulletSharp.Math.Vector3 ToBulletVector(this Microsoft.Xna.Framework.Vector3 vector)
        {
            return new BulletSharp.Math.Vector3(vector.X, vector.Y, vector.Z);
        }

        public static Microsoft.Xna.Framework.Vector3 ToXnaVector(this BulletSharp.Math.Vector3 vector)
        {
            return new Microsoft.Xna.Framework.Vector3(vector.X, vector.Y, vector.Z);
        }

        public static BulletSharp.Math.Matrix ToBulletMatrix(this Microsoft.Xna.Framework.Matrix matrix)
        {
            return new BulletSharp.Math.Matrix(matrix.M11, matrix.M12, matrix.M13, matrix.M14, 
                                               matrix.M21, matrix.M22, matrix.M23, matrix.M24, 
                                               matrix.M31, matrix.M32, matrix.M33, matrix.M34, 
                                               matrix.M41, matrix.M42, matrix.M43, matrix.M44);
        }

        public static Microsoft.Xna.Framework.Matrix ToXnaMatrix(this BulletSharp.Math.Matrix matrix)
        {
            return new Microsoft.Xna.Framework.Matrix(matrix.M11, matrix.M12, matrix.M13, matrix.M14,
                                                      matrix.M21, matrix.M22, matrix.M23, matrix.M24,
                                                      matrix.M31, matrix.M32, matrix.M33, matrix.M34,
                                                      matrix.M41, matrix.M42, matrix.M43, matrix.M44);
        }
    }
}
