﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Encompass3D.Utility
{
    public static class RNG
    {
        private static float SquareRootOfTwo = (float)Math.Sqrt(2f);

        private static int[] hash;
        private const int hashMask = 255;

        private static float[] gradients1D = {
            1f, -1f
        };

        private const int gradientsMask1D = 1;

        private static Vector2[] gradients2D = {
            new Vector2( 1f, 0f),
            new Vector2(-1f, 0f),
            new Vector2( 0f, 1f),
            new Vector2( 0f,-1f),
            Vector2.Normalize(new Vector2( 1f, 1f)),
            Vector2.Normalize(new Vector2(-1f, 1f)),
            Vector2.Normalize(new Vector2( 1f,-1f)),
            Vector2.Normalize(new Vector2(-1f,-1f))
        };

        private const int gradientsMask2D = 7;

        private static Vector3[] gradients3D = {
            new Vector3( 1f, 1f, 0f),
            new Vector3(-1f, 1f, 0f),
            new Vector3( 1f,-1f, 0f),
            new Vector3(-1f,-1f, 0f),
            new Vector3( 1f, 0f, 1f),
            new Vector3(-1f, 0f, 1f),
            new Vector3( 1f, 0f,-1f),
            new Vector3(-1f, 0f,-1f),
            new Vector3( 0f, 1f, 1f),
            new Vector3( 0f,-1f, 1f),
            new Vector3( 0f, 1f,-1f),
            new Vector3( 0f,-1f,-1f),

            new Vector3( 1f, 1f, 0f),
            new Vector3(-1f, 1f, 0f),
            new Vector3( 0f,-1f, 1f),
            new Vector3( 0f,-1f,-1f)
        };

        private const int gradientsMask3D = 15;


        private static Random random;

        public static void Initialize()
        {
            random = new Random();

            hash = new int[(hashMask + 1) * 2];

            for (int i = 0; i < hash.Length / 2; i++)
            {
                hash[i + (hash.Length / 2 - 1)] = hash[i] = RNG.GetInt(0, hashMask + 1);
            }

        }

        public static float GetFloat(float min = 0f, float max = 1f)
        {
            return (float)random.NextDouble() * (max - min) + min;
        }

        public static int GetInt(int min, int max)
        {
            return (int)Math.Floor(GetFloat(min, max));
        }

        public static Vector3 RandomInsideSphere(float radius)
        {
            float r = GetFloat(0f, radius);
            float theta = GetFloat(0f, MathHelper.TwoPi);
            float phi = GetFloat(0f, MathHelper.TwoPi);

            float x = r * (float)Math.Sin(theta) * (float)Math.Cos(phi);
            float y = r * (float)Math.Sin(theta) * (float)Math.Sin(phi);
            float z = r * (float)Math.Cos(theta);

            return new Vector3(x, y, z);
        }

        public static int GetIntInclusive(int min, int max)
        {
            return GetInt(min, max + 1);
        }

        public static T GetRandomItem<T>(this T[] array)
        {
            return array[GetInt(0, array.Length)];
        }

        public static XmlNode GetRandomNode(this XmlNodeList nodes)
        {
            return nodes[GetInt(0, nodes.Count)];
        }

        private static float Smooth(float t)
        {
            return t * t * t * (t * (t * 6f - 15f) + 10f);
        }

        public static float PerlinNoise(Vector2 point, float frequency, int octaves = 1, float persistence = 0.5f, float lacunarity = 2f)
        {
            float value = 0f;
            float amplitude = 1f;
            float range = 1f;

            for (int octave = 0; octave < octaves; octave++)
            {
                point *= frequency;
                int ix0 = (int)System.Math.Floor(point.X);
                int iy0 = (int)System.Math.Floor(point.Y);
                float tx0 = point.X - ix0;
                float ty0 = point.Y - iy0;
                float tx1 = tx0 - 1f;
                float ty1 = ty0 - 1f;
                ix0 &= hashMask;
                iy0 &= hashMask;
                int ix1 = ix0 + 1;
                int iy1 = iy0 + 1;


                int h0 = hash[ix0];
                int h1 = hash[ix1];
                Vector2 g00 = gradients2D[hash[h0 + iy0] & gradientsMask2D];
                Vector2 g10 = gradients2D[hash[h1 + iy0] & gradientsMask2D];
                Vector2 g01 = gradients2D[hash[h0 + iy1] & gradientsMask2D];
                Vector2 g11 = gradients2D[hash[h1 + iy1] & gradientsMask2D];

                float v00 = Vector2.Dot(g00, new Vector2(tx0, ty0));
                float v10 = Vector2.Dot(g10, new Vector2(tx1, ty0));
                float v01 = Vector2.Dot(g01, new Vector2(tx0, ty1));
                float v11 = Vector2.Dot(g11, new Vector2(tx1, ty1));

                float tx = Smooth(tx0);
                float ty = Smooth(ty0);

                value += MathHelper.SmoothStep(
                                    MathHelper.SmoothStep(v00, v10, tx),
                                    MathHelper.SmoothStep(v01, v11, tx),
                                    ty) * SquareRootOfTwo * amplitude;

                frequency *= lacunarity;
                amplitude *= persistence;
                range += amplitude;

            }

            return value / range;
        }

        private static float Dot(Vector3 g, float x, float y, float z)
        {
            return g.X * x + g.Y * y + g.Z * z;
        }

        public static float PerlinNoise3D(Vector3 point, float frequency, int octaves = 1, float persistence = 0.5f, float lacunarity = 2f)
        {
            float value = 0f;
            float amplitude = 1f;
            float range = 1f;

            for (int octave = 0; octave < octaves; octave++)
            {
                point *= frequency;
                int ix0 = (int)Math.Floor(point.X);
                int iy0 = (int)Math.Floor(point.Y);
                int iz0 = (int)Math.Floor(point.Z);
                float tx0 = point.X - ix0;
                float ty0 = point.Y - iy0;
                float tz0 = point.Z - iz0;
                float tx1 = tx0 - 1f;
                float ty1 = ty0 - 1f;
                float tz1 = tz0 - 1f;
                ix0 &= hashMask;
                iy0 &= hashMask;
                iz0 &= hashMask;
                int ix1 = ix0 + 1;
                int iy1 = iy0 + 1;
                int iz1 = iz0 + 1;

                int h0 = hash[ix0];
                int h1 = hash[ix1];
                int h00 = hash[h0 + iy0];
                int h10 = hash[h1 + iy0];
                int h01 = hash[h0 + iy1];
                int h11 = hash[h1 + iy1];
                Vector3 g000 = gradients3D[hash[h00 + iz0] & gradientsMask3D];
                Vector3 g100 = gradients3D[hash[h10 + iz0] & gradientsMask3D];
                Vector3 g010 = gradients3D[hash[h01 + iz0] & gradientsMask3D];
                Vector3 g110 = gradients3D[hash[h11 + iz0] & gradientsMask3D];
                Vector3 g001 = gradients3D[hash[h00 + iz1] & gradientsMask3D];
                Vector3 g101 = gradients3D[hash[h10 + iz1] & gradientsMask3D];
                Vector3 g011 = gradients3D[hash[h01 + iz1] & gradientsMask3D];
                Vector3 g111 = gradients3D[hash[h11 + iz1] & gradientsMask3D];

                float v000 = Dot(g000, tx0, ty0, tz0);
                float v100 = Dot(g100, tx1, ty0, tz0);
                float v010 = Dot(g010, tx0, ty1, tz0);
                float v110 = Dot(g110, tx1, ty1, tz0);
                float v001 = Dot(g001, tx0, ty0, tz1);
                float v101 = Dot(g101, tx1, ty0, tz1);
                float v011 = Dot(g011, tx0, ty1, tz1);
                float v111 = Dot(g111, tx1, ty1, tz1);

                float tx = Smooth(tx0);
                float ty = Smooth(ty0);
                float tz = Smooth(tz0);
                value += MathHelper.Lerp(
                    MathHelper.Lerp(MathHelper.Lerp(v000, v100, tx), MathHelper.Lerp(v010, v110, tx), ty),
                    MathHelper.Lerp(MathHelper.Lerp(v001, v101, tx), MathHelper.Lerp(v011, v111, tx), ty),
                    tz) * amplitude;

                frequency *= lacunarity;
                amplitude *= persistence;
                range += amplitude;
            }

            return value / range;
        }

    }
}
