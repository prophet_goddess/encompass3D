﻿using BulletSharp;
using System.Collections.Generic;

namespace Encompass3D.Utility
{
    public class ConvexResults : ConvexResultCallback
    {
        public List<LocalConvexResult> localConvexResults = new List<LocalConvexResult>();

        public override float AddSingleResult(LocalConvexResult convexResult, bool normalInWorldSpace)
        {
            localConvexResults.Add(convexResult);
            return 0;
        }
    }
}
