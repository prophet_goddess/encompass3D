using BulletSharp;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Encompass3D.Utility
{
    public class ContactResults : ContactResultCallback
    {
        private List<(CollisionObject, Vector3, Vector3)> collisions = new List<(CollisionObject, Vector3, Vector3)>();
        public CollisionObject TestingObject { get; set; } = null;

        public override float AddSingleResult(ManifoldPoint cp, CollisionObjectWrapper colObj0Wrap, int partId0, int index0, CollisionObjectWrapper colObj1Wrap, int partId1, int index1)
        {
            if (colObj0Wrap.CollisionObject == TestingObject)
            {
                collisions.Add((colObj1Wrap.CollisionObject, cp.PositionWorldOnB.ToXnaVector(), cp.NormalWorldOnB.ToXnaVector()));
            }
            else
            {
                collisions.Add((colObj0Wrap.CollisionObject, cp.PositionWorldOnB.ToXnaVector(), cp.NormalWorldOnB.ToXnaVector()));
            }

            return 0;
        }

        public void Clear()
        {
            collisions.Clear();
        }

        public IEnumerable<(CollisionObject, Vector3, Vector3)> CollidedObjects()
        {
            foreach (var collision in collisions)
            {
                yield return collision;
            }
        }
    }
}
